package com.tdespenza.sentenceserver.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
@Data
@AllArgsConstructor
public class Word {
    @NonNull
    private String value;
    private Role role;

    public enum Role {
        SUBJECT, VERB, ARTICLE, ADJECTIVE, NOUN;
    }
}
