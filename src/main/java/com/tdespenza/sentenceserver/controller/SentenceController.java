package com.tdespenza.sentenceserver.controller;

import com.tdespenza.sentenceserver.service.SentenceService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/15/17.
 */
@Controller
public class SentenceController {

    @Autowired
    private SentenceService sentenceService;

    @GetMapping("/sentence")
    public String getSentence() {
        /*val start = System.currentTimeMillis();
        val output = "<h3>Some Sentences</h3><br/>" +
                sentenceService.buildSentence() + "<br/><br/>" +
                sentenceService.buildSentence() + "<br/><br/>" +
                sentenceService.buildSentence() + "<br/><br/>" +
                sentenceService.buildSentence() + "<br/><br/>" +
                sentenceService.buildSentence() + "<br/><br/>";
        val end = System.currentTimeMillis();

        return output + "Elapsed time (ms): " + (end - start);*/

        return "sentence";
    }
}
