/**
 * Created by tdespenza on 1/18/17.
 */
// loose augmentation module pattern
var SentenceModule = (function (parent, $) {
  'use strict';

  //attributes
  var self = parent = parent || {};
  var prefix = '/services';

  // public functions
  self.init = function () {
    getJson('subject');
    getJson('verb');
    getJson('article');
    getJson('adjective');
    getJson('noun');
  };

  // private functions
  function getJson(partOfSpeech) {
    $.getJSON(prefix + '/' + partOfSpeech, null, function (results) {
      display(partOfSpeech, results);

    }).fail(function (data, status, xhr) {
      displayError(data, status, xhr, partOfSpeech);
    });
  }

  function display(id, results) {
    $('#' + id).html(results.value);
  }

  function displayError(data, status, xhr, type) {
    err(data, status, xhr, type);
  }

  function err(xhr, status, msg, type) {
    var error = '#error';
    var message = '<p><b>AJAX Failure retrieving ' + type + ': ' + xhr.status + ' ' + msg + '</b><br/></p>';

    $(error).append(message);
    $(error).show();
  }

  return parent;

})(SentenceModule || {}, jQuery);

//initialize module
$(function () {
  SentenceModule.init();
});
