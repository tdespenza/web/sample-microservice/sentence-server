package com.tdespenza.sentenceserver.client;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
@FeignClient("ARTICLE")
public interface ArticleClient extends PartOfSpeech {
}
