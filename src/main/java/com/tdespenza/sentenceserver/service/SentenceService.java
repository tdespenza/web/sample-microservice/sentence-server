package com.tdespenza.sentenceserver.service;

import com.tdespenza.sentenceserver.model.Sentence;
import com.tdespenza.sentenceserver.model.Word;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
@Service
@Log4j
public class SentenceService {

    @Autowired
    private WordService wordService;

    public String buildSentence() {
        final Sentence sentence = new Sentence();
        final List<Observable<Word>> observables = createObservables();
        final CountDownLatch latch = new CountDownLatch(observables.size());

        Observable.merge(observables).subscribe(word -> {
            sentence.add(word);
            latch.countDown();
        });

        waitForAll(latch);

        return sentence.toString();
    }

    private void waitForAll(final CountDownLatch latch) {
        try {
            latch.await();

        } catch (final InterruptedException e) {
            log.error(e.getMessage());
        }
    }

    private List<Observable<Word>> createObservables() {
        final List<Observable<Word>> observables = new ArrayList<>();
        observables.add(wordService.getSubject());
        observables.add(wordService.getVerb());
        observables.add(wordService.getArticle());
        observables.add(wordService.getAdjective());
        observables.add(wordService.getNoun());

        return observables;
    }
}
