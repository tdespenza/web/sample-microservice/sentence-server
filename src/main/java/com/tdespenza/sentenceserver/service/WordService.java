package com.tdespenza.sentenceserver.service;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import com.tdespenza.sentenceserver.client.*;
import com.tdespenza.sentenceserver.model.Word;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import rx.Observable;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
@Service
@Log4j
public class WordService {

    private static final String COMMAND_GROUP = "WORD";

    private VerbClient verbClient;
    private SubjectClient subjectClient;
    private ArticleClient articleClient;
    private AdjectiveClient adjectiveClient;
    private NounClient nounClient;

    public WordService(final VerbClient verbClient, final SubjectClient subjectClient,
                       final ArticleClient articleClient, final AdjectiveClient adjectiveClient,
                       final NounClient nounClient) {
        this.verbClient = verbClient;
        this.subjectClient = subjectClient;
        this.articleClient = articleClient;
        this.adjectiveClient = adjectiveClient;
        this.nounClient = nounClient;
    }

    @HystrixCommand(fallbackMethod = "verbFallback")
    public Observable<Word> getVerb() {
        return Observable.from(new AsyncResult() {
            @Override
            public Word invoke() {
                return verbClient.getWord();
            }
        });
    }

    @HystrixCommand(fallbackMethod = "subjectFallback")
    public Observable<Word> getSubject() {
        return Observable.from(new AsyncResult<Word>() {
            @Override
            public Word invoke() {
                return subjectClient.getWord();
            }
        });
    }

    @HystrixCommand(fallbackMethod = "articleFallback")
    public Observable<Word> getArticle() {
        return Observable.from(new AsyncResult<Word>() {
            @Override
            public Word invoke() {
                return articleClient.getWord();
            }
        });
    }

    @HystrixCommand(fallbackMethod = "adjectiveFallback")
    public Observable<Word> getAdjective() {
        return Observable.from(new AsyncResult<Word>() {
            @Override
            public Word invoke() {
                return adjectiveClient.getWord();
            }
        });
    }

    @HystrixCommand(fallbackMethod = "nounFallback")
    public Observable<Word> getNoun() {
        return Observable.from(new AsyncResult<Word>() {
            @Override
            public Word invoke() {
                return nounClient.getWord();
            }
        });
    }

    public Word verbFallback() {
        return new Word("does", Word.Role.VERB);
    }

    public Word subjectFallback() {
        return new Word("Someone", Word.Role.SUBJECT);
    }

    public Word articleFallback() {
        return new Word("", Word.Role.ARTICLE);
    }

    public Word adjectiveFallback() {
        return new Word("", Word.Role.ADJECTIVE);
    }

    public Word nounFallback() {
        return new Word("something", Word.Role.NOUN);
    }
}
